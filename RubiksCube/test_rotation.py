from Rotation import *

import numpy as np

# def inv(x): return [i * -1 for i in x]

xn = [1, 0, 0]
yn = [0, 1, 0]
zn = [0, 0, 1]

def test_rotation():
    uuts = ((Rotation.Xleft, ((xn, xn), (yn, zn), (zn, inv(yn)))),
            (Rotation.Yleft, ((xn, inv(zn)), (yn, yn), (zn, xn))),
            (Rotation.Zleft, ((xn, yn), (yn, inv(xn)), (zn, zn))),
            (Rotation.Xright, ((xn, xn), (yn, inv(zn)), (zn, yn))),
            (Rotation.Yright, ((xn, zn), (yn, yn), (zn, inv(xn)))),
            (Rotation.Zright, ((xn, inv(yn)), (yn, xn), (zn, zn)))
            )
    for uut in uuts:
        r = uut[0]
        for (t, e) in uut[1]:
            result = np.dot(t, r.value)
            assert np.array_equal(list(e), result)
