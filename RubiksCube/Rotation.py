from enum import Enum
def inv(x): return [i * -1 for i in x]
class Rotation(Enum):
    """
    The rotation matrix for a right handed coordinate system around each of the
    major axes
    """
    Xleft = ((1, 0, 0), (0, 0, 1), (0, -1, 0))
    Xright = ((1, 0, 0), (0, 0, -1), (0, 1, 0))
    Yleft = ((0, 0, -1), (0, 1, 0), (1, 0, 0))
    Yright = ((0, 0, 1), (0, 1, 0), (-1, 0, 0))
    Zleft = ((0, 1, 0), (-1, 0, 0), (0, 0, 1))
    Zright = ((0, -1, 0), (1, 0, 0), (0, 0, 1))
