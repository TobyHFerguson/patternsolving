# content of test_sample.py

from pytest import raises
from wordpuzzle import (Board, Move, InvalidMoveError,
                        NoHoleError, MultipleHolesError, new_neighbors_only,
                        neighbors_with_history, MyStream)


def test_get_neighors():
    assert top_left_two.get_neighbors() == {
        Move.Down: Board([[4, 2], [0, 5]]),
        Move.Right: Board([[2, 0], [4, 5]])
    }

    assert mid_mid.get_neighbors() == {
        Move.Up: Board([[1, 0, 3], [4, 2, 6], [7, 8, 9]]),
        Move.Down: Board([[1, 2, 3], [4, 8, 6], [7, 0, 9]]),
        Move.Left: Board([[1, 2, 3], [0, 4, 6], [7, 8, 9]]),
        Move.Right: Board([[1, 2, 3], [4, 6, 0], [7, 8, 9]])
    }


def test_simple_creation():
    assert Board([[1, 2, 3], [4, 0, 6], [7, 8, 9]])


def test_no_hole():
    with raises(NoHoleError):
        Board([[1, 2, 3], [4, 5, 6], [7, 8, 9]])


def test_hole_detection0():
    assert Board([[0, 2, 3], [4, 5, 6], [7, 8, 9]]).get_holes() == [(0, 0)]
    assert Board([[2, 0, 3], [4, 5, 6], [7, 8, 9]]).get_holes() == [(0, 1)]


nullboard = Board([[0]])
hboard = Board([[0, 2]])
vboard = Board([[0], [1]])
simple_twoby = Board([[0, 1], [1, 1]])
top_left_two = Board([[0, 2], [4, 5]])
top_left = Board([[0, 2, 3], [4, 5, 6], [7, 8, 9]])
mid_mid = Board([[1, 2, 3], [4, 0, 6], [7, 8, 9]])
bottom_right = Board([[1, 2, 3], [4, 5, 6], [7, 8, 0]])
bottom_right_two = Board([[1, 2], [4, 0]])


def test_str():
    assert str(top_left) == "023456789"


def test_get_moves_null():
    assert nullboard.get_moves() == []


def test_get_moves_hboard():
    assert hboard.get_moves() == [Move.Right]


def test_get_moves_vboard():
    assert vboard.get_moves() == [Move.Down]


def test_multiple_holes():
    with raises(MultipleHolesError):
        Board([[0, 0]])


def test_get_moves_top_left():
    assert top_left.get_moves() == [Move.Down, Move.Right]
    assert top_left_two.get_moves() == [Move.Down, Move.Right]
    assert mid_mid.get_moves() == [Move.Up, Move.Down, Move.Left, Move.Right]
    assert bottom_right.get_moves() == [Move.Up, Move.Left]
    assert bottom_right_two.get_moves() == [Move.Up, Move.Left]


def test_equality():
    assert top_left != mid_mid
    assert bottom_right_two.get_moves() == [Move.Up, Move.Left]


def test_up():
    uut = mid_mid
    res = mid_mid.move(Move.Up)
    assert res == Board(
        [[1, 0, 3], [4, 2, 6], [7, 8, 9]])
    assert uut is not res


def test_invalid_up():
    with raises(InvalidMoveError):
        top_left.move(Move.Up)
    with raises(InvalidMoveError):
        nullboard.move(Move.Up)
    with raises(InvalidMoveError):
        hboard.move(Move.Up)


def test_down():
    uut = top_left
    result = uut.move(Move.Down)
    expected = Board(
        [[4, 2, 3], [0, 5, 6], [7, 8, 9]])
    assert expected == result


def test_invalid_down():
    with raises(InvalidMoveError):
        bottom_right.move(Move.Down)
    with raises(InvalidMoveError):
        nullboard.move(Move.Down)
    with raises(InvalidMoveError):
        hboard.move(Move.Down)


def test_left():
    uut = bottom_right
    expected = Board([[1, 2, 3], [4, 5, 6], [7, 0, 8]])
    assert expected == uut.move(Move.Left)


def test_invalid_left():
    with raises(InvalidMoveError):
        top_left.move(Move.Left)
    with raises(InvalidMoveError):
        nullboard.move(Move.Left)
    with raises(InvalidMoveError):
        hboard.move(Move.Left)
    with raises(InvalidMoveError):
        vboard.move(Move.Left)


def test_right():
    uut = top_left
    expected = Board([[2, 0, 3], [4, 5, 6], [7, 8, 9]])
    assert expected == uut.move(Move.Right)


def test_invalid_right():
    with raises(InvalidMoveError):
        bottom_right.move(Move.Right)
    with raises(InvalidMoveError):
        nullboard.move(Move.Right)
    with raises(InvalidMoveError):
        vboard.move(Move.Right)


def test_neighbors_with_history_simple():
    assert set(neighbors_with_history(top_left_two, ())) == set([
        (Board([[4, 2], [0, 5]]), (Move.Down,)),
        (Board([[2, 0], [4, 5]]), (Move.Right,))
    ])


def test_neighbors_with_top_middle_simpl():
    uut = Board([[1, 0, 1], [1, 1, 1], [1, 1, 1]])
    expected = [(Board([[1, 1, 1], [1, 0, 1], [1, 1, 1]]), (Move.Down,)),
                (Board([[0, 1, 1], [1, 1, 1], [1, 1, 1]]), (Move.Left,)),
                (Board([[1, 1, 0], [1, 1, 1], [1, 1, 1]]), (Move.Right,))
                ]
    assert expected == [i for i in neighbors_with_history(uut, ())]


def test_neighbors_with_history_triple():
    assert set(neighbors_with_history(mid_mid, ())) == set(
        [(Board([[1, 2, 3], [4, 6, 0], [7, 8, 9]]), (Move.Right,)),
         (Board([[1, 2, 3], [0, 4, 6], [7, 8, 9]]), (Move.Left,)),
         (Board([[1, 0, 3], [4, 2, 6], [7, 8, 9]]), (Move.Up,)),
         (Board([[1, 2, 3], [4, 8, 6], [7, 0, 9]]), (Move.Down,))]
    )


def test_new_neighbors_only():
    assert set(new_neighbors_only(neighbors_with_history(mid_mid, ()), {Board([[1, 2, 3], [4, 6, 0], [7, 8, 9]]), (Move.Right,)})) == set(
        [(Board([[1, 2, 3], [0, 4, 6], [7, 8, 9]]), (Move.Left,)),
         (Board([[1, 0, 3], [4, 2, 6], [7, 8, 9]]), (Move.Up,)),
         (Board([[1, 2, 3], [4, 8, 6], [7, 0, 9]]), (Move.Down,))]
    )


def test_new_neighbors_only_no_history():
    assert set(new_neighbors_only(neighbors_with_history(mid_mid, ()), {Board([[1, 2, 3], [4, 6, 0], [7, 8, 9]]), (Move.Right,)})) == set(
        [(Board([[1, 2, 3], [0, 4, 6], [7, 8, 9]]), (Move.Left,)),
         (Board([[1, 0, 3], [4, 2, 6], [7, 8, 9]]), (Move.Up,)),
         (Board([[1, 2, 3], [4, 8, 6], [7, 0, 9]]), (Move.Down,))]
    )


def test_new_neighbors_only_empty_generator():
    assert set(new_neighbors_only(
        (i for i in []), ())) == set()


def test_allBoards_on_pathalogical_board():
    bhg = ((b, h) for (b, h) in [(nullboard, ())])
    mystream = MyStream(bhg, set())
    assert [i for i in mystream] == [(nullboard, ())]


def test_allBoards_on_horizontal_board():
    bhg = ((b, h) for (b, h) in [(hboard, ())])
    mystream = MyStream(bhg, set())
    assert next(mystream) == (Board([[0, 2]]), ())
    assert next(mystream) == (Board([[2, 0]]), (Move.Right,))
    assert mystream.explored == set([Board([[0, 2]]), Board([[2, 0]])])
    with raises(StopIteration):
        next(mystream)


def test_allBoards_on_vboard():
    bhg = ((b, h) for (b, h) in [(vboard, ())])
    mystream = MyStream(bhg, set())
    assert [i for i in mystream] == \
        [(Board([[0], [1]]), ()),
         (Board([[1], [0]]), (Move.Down,))]


def test_allBoards_on_simpleboard():
    bhg = ((b, h) for (b, h) in [(simple_twoby, ())])
    mystream = MyStream(bhg, set())
    result = [i for i in mystream]
    r0 = (simple_twoby, ())
    r1 = (Board([[1, 1], [0, 1]]), (Move.Down,))
    r2 = (Board([[1, 0], [1, 1]]), (Move.Right,))
    r3 = (Board([[1, 1], [1, 0]]), (Move.Down, Move.Right))
    assert 4 == len(result)
    assert set(result) == set([r0, r1, r2, r3])
    assert result[0] == r0
    assert result[1] == r1
    assert result[2] == r2
    assert result[3] == r3


def test_with_simple_rep():
    bhg = ((b, h) for (b, h) in [(top_left_two, ())])
    mystream = MyStream(bhg, set())
    result = [i for i in mystream]
    expected = ["(0245, [])",
                "(4205, ['d'])",
                "(2045, ['r'])",
                "(4250, ['d', 'r'])",
                "(2540, ['r', 'd'])",
                "(4052, ['d', 'r', 'u'])",
                "(2504, ['r', 'd', 'l'])",
                "(0452, ['d', 'r', 'u', 'l'])",
                "(0524, ['r', 'd', 'l', 'u'])",
                "(5402, ['d', 'r', 'u', 'l', 'd'])",
                "(5024, ['r', 'd', 'l', 'u', 'r'])",
                "(5420, ['d', 'r', 'u', 'l', 'd', 'r'])"
                ]
    for i in range(0, len(result) - 1):
        (b, m) = result[i]
        assert "({}, {})".format(b, [str(i) for i in m]) == expected[i]


def test_shortest_path():
    board = Board([[0, 1, 1], [1, 1, 1], [1, 1, 1]])
    bhg = ((b, h) for (b, h) in [(board, ())])
    mystream = MyStream(bhg, set())
    result = [i for i in mystream]
    expected = [
        "(011111111, [])",
        "(111011111, ['d'])",
        "(101111111, ['r'])",
        "(111111011, ['d', 'd'])",
        "(111101111, ['d', 'r'])",
        "(110111111, ['r', 'r'])",
        "(111111101, ['d', 'd', 'r'])",
        "(111110111, ['r', 'r', 'd'])"
        "(111111110, ['d', 'd', 'r', 'r'])"
    ]
    for i in range(0, len(expected) - 1):
        (b, m) = result[i]
        assert "({}, {})".format(b, [str(i) for i in m]) == expected[i]


def test_shortest_path_in_parts():
    board = Board([[0, 1, 1], [1, 1, 1], [1, 1, 1]])
    bhg = ((b, h) for (b, h) in [(board, ())])
    mystream = MyStream(bhg, set())
    e = set()
    (b, h) = next(mystream)
    assert (b, h) == (Board([[0, 1, 1], [1, 1, 1], [1, 1, 1]]), ())
    e.add(b)
    assert mystream.explored == e
    (b1, h1) = next(mystream)
    assert (b1, h1) == (Board([[1, 1, 1], [0, 1, 1], [1, 1, 1]]), (Move.Down,))
    e.add(b1)
    assert mystream.explored == e
    (b2, h2) = next(mystream)
    assert (b2, h2) == (
        Board([[1, 0, 1], [1, 1, 1], [1, 1, 1]]), (Move.Right,))
    e.add(b2)
    assert mystream.explored == e


def test_solution():
    board = Board([[0, 1, 1], [1, 1, 1], [1, 1, 1]])
    bhg = ((b, h) for (b, h) in [(board, ())])
    mystream = MyStream(bhg, set())
    end = Board([[1, 1, 1], [1, 1, 1], [1, 1, 0]])

    result = [h for (b, h) in mystream if b == end]
    assert result == [(Move.Down, Move.Down, Move.Right, Move.Right)]
