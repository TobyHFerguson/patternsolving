from cube import (Color, Cube, Cubelet, Face, OppositeFacesError, Rotation,
                  SameColorDifferentOrientationError, SameFacesError,
                  SameOrientationError)
from pytest import raises
import numpy as np


def inv(x): return [i * -1 for i in x]


xn = [1, 0, 0]
yn = [0, 1, 0]
zn = [0, 0, 1]


def test_face_construction():
    assert Face(Color.Blue, xn) is not None


def test_rotation():
    uuts = ((Rotation.Xleft, ((xn, xn), (yn, zn), (zn, inv(yn)))),
            (Rotation.Yleft, ((xn, inv(zn)), (yn, yn), (zn, xn))),
            (Rotation.Zleft, ((xn, yn), (yn, inv(xn)), (zn, zn))),
            (Rotation.Xright, ((xn, xn), (yn, inv(zn)), (zn, yn))),
            (Rotation.Yright, ((xn, zn), (yn, yn), (zn, inv(xn)))),
            (Rotation.Zright, ((xn, inv(yn)), (yn, xn), (zn, zn)))
            )
    for uut in uuts:
        r = uut[0]
        for (t, e) in uut[1]:
            result = np.dot(t, r.value)
            assert np.array_equal(list(e), result)
#            assert (e == result).all()


def test_face_rotation_xleft_x():
    uut = Face(Color.Blue, xn)
    expected = Face(Color.Blue, xn)
    uut.rotate(Rotation.Xleft)
    assert np.array_equal(uut.orientation, expected.orientation)


def test_face_rotation_xleft_y():
    uut = Face(Color.Blue, yn)
    expected = Face(Color.Blue, zn)
    assert expected == uut.rotate(Rotation.Xleft)


def test_face_rotation_xleft_z():
    uut = Face(Color.Blue, zn)
    expected = Face(Color.Blue, inv(yn))
    assert expected == uut.rotate(Rotation.Xleft)


def test_face_rotation_xright_x():
    uut = Face(Color.Blue, xn)
    expected = Face(Color.Blue, xn)
    assert expected == uut.rotate(Rotation.Xright)


def test_face_rotation_xright_y():
    uut = Face(Color.Blue, yn)
    expected = Face(Color.Blue, inv(zn))
    assert expected == uut.rotate(Rotation.Xright)
    assert expected is not uut.rotate(Rotation.Xright)


def test_face_opposition_1():
    uut = Face(Color.Blue, yn)
    assert uut.isOpposite(Face(Color.Green, inv(yn)))


def test_face_opposition_2():
    uut = Face(Color.Blue, yn)
    assert not uut.isOpposite(uut)


def test_face_oppostion_3():
    uut = Face(Color.Blue, yn)
    assert not uut.isOpposite(None)


def test_face_opposition_4():
    face1 = Face(Color.Blue, xn)
    face2 = Face(Color.Green, inv(xn))
    assert face1.isOpposite(face2)


def test_distinct_faces_have_distinct_hash_values():
    """
    Ensure that all Color/Orientations hash differently
    """
    orientation = [xn, yn, zn, inv(xn), inv(yn), inv(zn)]
    faces1 = [Face(c, o) for c in Color for o in orientation]
    faces2 = [Face(c, o) for c in Color for o in orientation]
    for i in range(0, len(faces1)):
        for j in range(0, len(faces1)):
            if i != j:
                assert hash(faces1[i]) != hash(faces2[j])


def test_face_equality():
    """
    Two faces are equal iff their hashes are equal
    """
    orientation = [xn, yn, zn, inv(xn), inv(yn), inv(zn)]
    faces1 = [Face(c, o) for c in Color for o in orientation]
    faces2 = [Face(c, o) for c in Color for o in orientation]
    for i in range(0, len(faces1)):
        for j in range(0, len(faces1)):
            if i != j:
                assert hash(faces1[i]) != hash(
                    faces2[j]) and faces1[i] != faces2[j]
            else:
                assert hash(faces1[i]) == hash(
                    faces2[j]) and faces1[i] == faces2[j]


def test_cubelet_construction_1():
    assert Cubelet(Face(Color.Blue, xn)) is not None


def test_cubelet_construction_2():
    face1 = Face(Color.Blue, xn)
    face2 = Face(Color.Green, yn)
    assert Cubelet(face1, face2) is not None


def test_cubelet_construction_3():
    face1 = Face(Color.Blue, xn)
    face2 = Face(Color.Green, yn)
    face3 = Face(Color.Yellow, zn)
    assert Cubelet(face1, face2, face3) is not None


def test_invalid_cubelet_construction_colors():
    face1 = Face(Color.Blue, xn)
    face2 = Face(Color.Blue, yn)
    with raises(SameColorDifferentOrientationError):
        Cubelet(face1, face2)


def test_invalid_cubelet_construction_orientation():
    face1 = Face(Color.Blue, xn)
    face2 = Face(Color.Green, inv(xn))
    with raises(OppositeFacesError):
        Cubelet(face1, face2)


def test_cubelet_faces_count():
    uut = Cubelet(Face(Color.Blue, xn), Face(Color.Green, yn))
    assert 2 == len(uut.faces)


def test_invalid_cubelet_construction_same_orientation():
    face1 = Face(Color.Blue, xn)
    face2 = Face(Color.Green, xn)
    with raises(SameOrientationError):
        Cubelet(face1, face2)


def test_invalid_cubelet_construction_faces():
    face1 = Face(Color.Blue, xn)
    with raises(SameFacesError):
        Cubelet(face1, face1)


def sameOrientation(lhs, rhs):
    return np.array_equal(lhs, rhs)


def test_cubelet_rotation_x_left():
    face1 = Face(Color.Green, xn)
    face2 = Face(Color.Blue, yn)
    face3 = Face(Color.White, zn)
    uut = Cubelet(face1, face2, face3).rotate(Rotation.Xleft)
    assert sameOrientation(uut.getFaceWithColor(Color.Green).orientation, xn)
    assert sameOrientation(uut.getFaceWithColor(Color.Blue).orientation, zn)
    assert sameOrientation(uut.getFaceWithColor(
        Color.White).orientation, inv(yn))


def test_cubelet_rotation_has_no_side_effects():
    c1 = Cubelet(Face(Color.Green, xn))
    c2 = c1.rotate(Rotation.Xleft)
    assert c1 == c2
    assert c1 is not c2
    c3 = c1.rotate(Rotation.Yleft)
    assert c1 != c3
    assert c1 is not c3
    assert c2 != c3
    assert c2 is not c3


def test_CubeConstruction():
    assert Cube() is not None


def test_cubes_composed_of_many_parts():
    assert len(Cube().getCubelets()) == 26


def test_CubeConstruction_1():
    cube = Cube()
    assert cube.getCubelets()[(-1, -1, -1)] is not None
    assert cube.getCubelets()[(1, 0, -1)] is not None


def test_cubelet_equality():
    face1 = Face(Color.Blue, xn)
    face2 = Face(Color.Green, yn)
    assert Cubelet(face1, face2) == Cubelet(face1, face2)


def test_cubelet_inequality():
    face1 = Face(Color.Blue, xn)
    face2 = Face(Color.Green, yn)
    assert Cubelet(face1, face2) != Cubelet(face1)


def test_cubes_composed_of_cubes():
    cube = Cube()
    assert len(cube.getCubelets()) == 26


def test_cube_hash():
    cube1 = Cube()
    cube2 = Cube()
    assert hash(cube1) == hash(cube2)


def test_cube_copy_constructor():
    cube = Cube()
    assert cube == Cube(cube.getCubelets())


def test_get_keys_in_layer():
    cube = Cube()
    uut = cube.getCubeletLocationsInLayer(1, Rotation.Xleft)
    assert (1, 0, 0) in uut
    assert 9 == len(uut)
    assert None not in uut


def test_get_keys_access():
    cube1 = Cube()
    cube2 = Cube()
    keys = cube1.getCubeletLocationsInLayer(1, Rotation.Xleft)
    values = [cube2.getCubelets()[k] for k in keys]
    assert None not in values


def test_cube_corner_111_is_correct():
    uut = Cube()
    c1 = uut.getCubelets()[(1, 1, 1)]
    assert Face(Color.Green, xn) in c1.faces
    assert Face(Color.Blue, yn) in c1.faces
    assert Face(Color.White, zn) in c1.faces


def test_cube_corner_11_1_is_correct():
    uut = Cube()
    c1 = uut.getCubelets()[(1, 1, -1)]
    assert Face(Color.Green, xn) in c1.faces
    assert Face(Color.Blue, yn) in c1.faces
    assert Face(Color.Purple, inv(zn)) in c1.faces


def test_cube_corner_1_11_is_correct():
    uut = Cube()
    c1 = uut.getCubelets()[(1, -1, 1)]
    assert Face(Color.Green, xn) in c1.faces
    assert Face(Color.Red, inv(yn)) in c1.faces
    assert Face(Color.White, zn) in c1.faces


def test_cube_corner_1_1_1_is_correct():
    uut = Cube()
    c1 = uut.getCubelets()[(1, -1, -1)]
    assert Face(Color.Green, xn) in c1.faces
    assert Face(Color.Red, inv(yn)) in c1.faces
    assert Face(Color.Purple, inv(zn)) in c1.faces


def test_cube_corner__111_is_correct():
    uut = Cube()
    c1 = uut.getCubelets()[(-1, 1, 1)]
    assert Face(Color.Yellow, inv(xn)) in c1.faces
    assert Face(Color.Blue, yn) in c1.faces
    assert Face(Color.White, zn) in c1.faces


def test_cube_corner__11_1_is_correct():
    uut = Cube()
    c1 = uut.getCubelets()[(-1, 1, -1)]
    assert Face(Color.Yellow, inv(xn)) in c1.faces
    assert Face(Color.Blue, yn) in c1.faces
    assert Face(Color.Purple, inv(zn)) in c1.faces


def test_cube_corner__1_11_is_correct():
    uut = Cube()
    c1 = uut.getCubelets()[(-1, -1, 1)]
    assert Face(Color.Yellow, inv(xn)) in c1.faces
    assert Face(Color.Red, inv(yn)) in c1.faces
    assert Face(Color.White, zn) in c1.faces


def test_cube_corner__1_1_1_is_correct():
    uut = Cube()
    c1 = uut.getCubelets()[(-1, -1, -1)]
    assert Face(Color.Yellow, inv(xn)) in c1.faces
    assert Face(Color.Red, inv(yn)) in c1.faces
    assert Face(Color.Purple, inv(zn)) in c1.faces


def test_cube_rotation_x1_left():
    uut = Cube()
    result = uut.rotate(1, Rotation.Xleft)
    assert uut != result
    result_cubelets = result.getCubelets()
    # Ensure that all nine cubes have been rotated correctly,
    # in both space and orientation
    assert result_cubelets[(1, 1, 1)] == Cubelet(Face(Color.Green, xn),
                                                 Face(Color.Blue, zn),
                                                 Face(Color.Purple, yn))
    assert result_cubelets[(1, 1, 0)] == Cubelet(Face(Color.Green, xn),
                                                 Face(Color.Purple, yn))
    assert result_cubelets[(1, 1, -1)] == Cubelet(Face(Color.Green, xn),
                                                  Face(Color.Red, inv(zn)),
                                                  Face(Color.Purple, yn))
    assert result_cubelets[(1, 0, 1)] == Cubelet(Face(Color.Green, xn),
                                                 Face(Color.Blue, zn))
    assert result_cubelets[(1, 0, 0)] == Cubelet(Face(Color.Green, xn))
    assert result_cubelets[(1, 0, -1)] == Cubelet(Face(Color.Green, xn),
                                                  Face(Color.Red, inv(zn)))
    assert result_cubelets[(1, -1, 1)] == Cubelet(Face(Color.Green, xn),
                                                  Face(Color.Blue, zn),
                                                  Face(Color.White, inv(yn)))
    assert result_cubelets[(1, -1, 0)] == Cubelet(Face(Color.Green, xn),
                                                  Face(Color.White, inv(yn)))
    assert result_cubelets[(1, -1, -1)] == Cubelet(Face(Color.Green, xn),
                                                   Face(Color.Red, inv(zn)),
                                                   Face(Color.White, inv(yn)))
    # Ensure that everything not on the plane x = 1 is unchanged
    assert uut.getCubeletsInLayer(0, Rotation.Xleft) == \
        result.getCubeletsInLayer(0, Rotation.Xleft)
    assert uut.getCubeletsInLayer(-1, Rotation.Xleft) == \
        result.getCubeletsInLayer(-1, Rotation.Xleft)


def test_cube_rotation_x_left_x_right():
    uut = Cube()
    result = uut.rotate(1, Rotation.Xleft).rotate(
        1, Rotation.Xright)
    assert uut == result


def test_cube_rotation_x_left2_x_right2():
    uut = Cube()
    left = uut.rotate(1, Rotation.Xleft).rotate(
        1, Rotation.Xleft)
    right = uut.rotate(1, Rotation.Xright).rotate(
        1, Rotation.Xright)
    assert left == right
    assert left != uut
    assert right != uut


def test_cube_rotation_x_left_four_times():
    uut = Cube()
    result = uut.rotate(1, Rotation.Xleft)
    for i in range(0, 3):
        result = result.rotate(1, Rotation.Xleft)
    assert uut == result
    assert uut.getCubelets()[1, 1, 1] == result.getCubelets()[1, 1, 1]
    assert uut.getCubelets() == result.getCubelets()


def test_cube_rotation_x0_left():
    uut = Cube()
    result = uut.rotate(0, Rotation.Xleft)
    # Ensure that all 8 cubes have been rotated correctly,
    # in both space and orientation
    result_cubelets = result.getCubelets()
    assert result_cubelets[(0, 1, 1)] == Cubelet(Face(Color.Blue, zn),
                                                 Face(Color.Purple, yn))
    assert result_cubelets[(0, 1, 0)] == Cubelet(Face(Color.Purple, yn))
    assert result_cubelets[(0, 1, -1)] == Cubelet(Face(Color.Red, inv(zn)),
                                                  Face(Color.Purple, yn))
    assert result_cubelets[(0, 0, 1)] == Cubelet(Face(Color.Blue, zn))
    assert result_cubelets[(0, 0, -1)] == Cubelet(Face(Color.Red, inv(zn)))
    assert result_cubelets[(0, -1, 1)] == Cubelet(Face(Color.Blue, zn),
                                                  Face(Color.White, inv(yn)))
    assert result_cubelets[(0, -1, 0)] == Cubelet(Face(Color.White, inv(yn)))
    assert result_cubelets[(0, -1, -1)] == Cubelet(Face(Color.Red, inv(zn)),
                                                   Face(Color.White, inv(yn)))
    # Ensure that everything not on the plane x = 1 is unchanged
    assert uut.getCubeletsInLayer(1, Rotation.Xleft) == \
        result.getCubeletsInLayer(1, Rotation.Xleft)
    assert uut.getCubeletsInLayer(-1, Rotation.Xleft) == \
        result.getCubeletsInLayer(-1, Rotation.Xleft)


def test_cube_rotation_x_1_left():
    uut = Cube()
    result = uut.rotate(-1, Rotation.Xleft)
    result_cubelets = result.getCubelets()
    # Ensure that all nine cubes have been rotated correctly,
    # in both space and orientation
    assert result_cubelets[(-1, 1, 1)] == Cubelet(Face(Color.Yellow, inv(xn)),
                                                  Face(Color.Blue, zn),
                                                  Face(Color.Purple, yn))
    assert result_cubelets[(-1, 1, 0)] == Cubelet(Face(Color.Yellow, inv(xn)),
                                                  Face(Color.Purple, yn))
    assert result_cubelets[(-1, 1, -1)] == Cubelet(Face(Color.Yellow, inv(xn)),
                                                   Face(Color.Red, inv(zn)),
                                                   Face(Color.Purple, yn))
    assert result_cubelets[(-1, 0, 1)] == Cubelet(Face(Color.Yellow, inv(xn)),
                                                  Face(Color.Blue, zn))
    assert result_cubelets[(-1, 0, 0)] == Cubelet(Face(Color.Yellow, inv(xn)))
    assert result_cubelets[(-1, 0, -1)] == Cubelet(Face(Color.Yellow, inv(xn)),
                                                   Face(Color.Red, inv(zn)))
    assert result_cubelets[(-1, -1, 1)] == Cubelet(Face(Color.Yellow, inv(xn)),
                                                   Face(Color.Blue, zn),
                                                   Face(Color.White, inv(yn)))
    assert result_cubelets[(-1, -1, 0)] == Cubelet(Face(Color.Yellow, inv(xn)),
                                                   Face(Color.White, inv(yn)))
    assert result_cubelets[(-1, -1, -1)] == Cubelet(Face(Color.Yellow, inv(xn)),
                                                    Face(Color.Red, inv(zn)),
                                                    Face(Color.White, inv(yn)))
    # Ensure that everything not on the plane x = 1 is unchanged
    assert uut.getCubeletsInLayer(0, Rotation.Xleft) == \
        result.getCubeletsInLayer(0, Rotation.Xleft)
    assert uut.getCubeletsInLayer(1, Rotation.Xleft) == \
        result.getCubeletsInLayer(1, Rotation.Xleft)


def test_cube_rotation_y1_left():
    uut = Cube()
    result = uut.rotate(1, Rotation.Yleft)
    result_cubelets = result.getCubelets()
    # Ensure that all nine cubes have been rotated correctly,
    # in both space and orientation
    assert result_cubelets[(1, 1, 1)] == Cubelet(Face(Color.Blue, yn),
                                                 Face(Color.Yellow, zn),
                                                 Face(Color.White, xn))
    assert result_cubelets[(1, 1, 0)] == Cubelet(Face(Color.Blue, yn),
                                                 Face(Color.White, xn))
    assert result_cubelets[(1, 1, -1)] == Cubelet(Face(Color.Blue, yn),
                                                  Face(Color.Green, inv(zn)),
                                                  Face(Color.White, xn))
    assert result_cubelets[(0, 1, 1)] == Cubelet(Face(Color.Blue, yn),
                                                 Face(Color.Yellow, zn))
    assert result_cubelets[(0, 1, 0)] == Cubelet(Face(Color.Blue, yn))
    assert result_cubelets[(0, 1, -1)] == Cubelet(Face(Color.Blue, yn),
                                                  Face(Color.Green, inv(zn)))
    assert result_cubelets[(-1, 1, 1)] == Cubelet(Face(Color.Blue, yn),
                                                  Face(Color.Yellow, zn),
                                                  Face(Color.Purple, inv(xn)))
    assert result_cubelets[(-1, 1, 0)] == Cubelet(Face(Color.Blue, yn),
                                                  Face(Color.Purple, inv(xn)))
    assert result_cubelets[(-1, 1, -1)] == Cubelet(Face(Color.Blue, yn),
                                                   Face(Color.Green, inv(zn)),
                                                   Face(Color.Purple, inv(xn)))
    # Ensure that everything not on the plane x = 1 is unchanged
    assert uut.getCubeletsInLayer(0, Rotation.Yleft) == \
        result.getCubeletsInLayer(0, Rotation.Yleft)
    assert uut.getCubeletsInLayer(-1, Rotation.Yleft) == \
        result.getCubeletsInLayer(-1, Rotation.Yleft)


def test_cube_rotation_y0_left():
    uut = Cube()
    result = uut.rotate(0, Rotation.Yleft)
    result_cubelets = result.getCubelets()
    # Ensure that all nine cubes have been rotated correctly,
    # in both space and orientation
    assert result_cubelets[(1, 0, 1)] == Cubelet(Face(Color.Yellow, zn),
                                                 Face(Color.White, xn))
    assert result_cubelets[(1, 0, 0)] == Cubelet(Face(Color.White, xn))
    assert result_cubelets[(1, 0, -1)] == Cubelet(Face(Color.Green, inv(zn)),
                                                  Face(Color.White, xn))
    assert result_cubelets[(0, 0, 1)] == Cubelet(Face(Color.Yellow, zn))
    assert result_cubelets[(0, 0, -1)] == Cubelet(Face(Color.Green, inv(zn)))
    assert result_cubelets[(-1, 0, 1)] == Cubelet(Face(Color.Yellow, zn),
                                                  Face(Color.Purple, inv(xn)))
    assert result_cubelets[(-1, 0, 0)] == Cubelet(Face(Color.Purple, inv(xn)))
    assert result_cubelets[(-1, 0, -1)] == Cubelet(Face(Color.Green, inv(zn)),
                                                   Face(Color.Purple, inv(xn)))
    # Ensure that everything not on the plane x = 1 is unchanged
    assert uut.getCubeletsInLayer(1, Rotation.Yleft) == \
        result.getCubeletsInLayer(1, Rotation.Yleft)
    assert uut.getCubeletsInLayer(-1, Rotation.Yleft) == \
        result.getCubeletsInLayer(-1, Rotation.Yleft)


def test_cube_rotation_y_1_left():
    uut = Cube()
    result = uut.rotate(-1, Rotation.Yleft)
    result_cubelets = result.getCubelets()
    # Ensure that all nine cubes have been rotated correctly,
    # in both space and orientation
    assert result_cubelets[(1, -1, 1)] == Cubelet(Face(Color.Red, inv(yn)),
                                                  Face(Color.Yellow, zn),
                                                  Face(Color.White, xn))
    assert result_cubelets[(1, -1, 0)] == Cubelet(Face(Color.Red, inv(yn)),
                                                  Face(Color.White, xn))
    assert result_cubelets[(1, -1, -1)] == Cubelet(Face(Color.Red, inv(yn)),
                                                   Face(Color.Green, inv(zn)),
                                                   Face(Color.White, xn))
    assert result_cubelets[(0, -1, 1)] == Cubelet(Face(Color.Red, inv(yn)),
                                                  Face(Color.Yellow, zn))
    assert result_cubelets[(0, -1, 0)] == Cubelet(Face(Color.Red, inv(yn)))
    assert result_cubelets[(0, -1, -1)] == Cubelet(Face(Color.Red, inv(yn)),
                                                   Face(Color.Green, inv(zn)))
    assert result_cubelets[(-1, -1, 1)] == Cubelet(Face(Color.Red, inv(yn)),
                                                   Face(Color.Yellow, zn),
                                                   Face(Color.Purple, inv(xn)))
    assert result_cubelets[(-1, -1, 0)] == Cubelet(Face(Color.Red, inv(yn)),
                                                   Face(Color.Purple, inv(xn)))
    assert result_cubelets[(-1, -1, -1)] == Cubelet(Face(Color.Red, inv(yn)),
                                                    Face(Color.Green, inv(zn)),
                                                    Face(Color.Purple, inv(xn)))
    # Ensure that everything not on the plane x = 1 is unchanged
    assert uut.getCubeletsInLayer(0, Rotation.Yleft) == \
        result.getCubeletsInLayer(0, Rotation.Yleft)
    assert uut.getCubeletsInLayer(1, Rotation.Yleft) == \
        result.getCubeletsInLayer(1, Rotation.Yleft)


def test_cube_rotation_y_left_y_right():
    uut = Cube()
    result = uut.rotate(1, Rotation.Yleft).rotate(
        1, Rotation.Yright)
    assert uut == result


def test_cube_rotation_y_left2_y_right2():
    uut = Cube()
    left = uut.rotate(1, Rotation.Yleft).rotate(
        1, Rotation.Yleft)
    right = uut.rotate(1, Rotation.Yright).rotate(
        1, Rotation.Yright)
    assert left == right


def test_cube_rotation_y_left_four_times():
    uut = Cube()
    result = uut.rotate(1, Rotation.Yleft)
    for i in range(0, 3):
        result = result.rotate(1, Rotation.Yleft)
    assert uut == result
    assert uut.getCubelets()[1, 1, 1] == result.getCubelets()[1, 1, 1]
    assert uut.getCubelets() == result.getCubelets()


def test_cube_rotation_z_left_z_right():
    uut = Cube()
    result = uut.rotate(1, Rotation.Xleft).rotate(
        1, Rotation.Xright)
    assert uut == result


def test_cube_rotation_z_left2_z_right2():
    uut = Cube()
    left = uut.rotate(1, Rotation.Xleft).rotate(
        1, Rotation.Xleft)
    right = uut.rotate(1, Rotation.Xright).rotate(
        1, Rotation.Xright)
    assert left == right


def test_cube_rotation_z_left_four_times():
    uut = Cube()
    result = uut.rotate(1, Rotation.Xleft)
    for i in range(0, 3):
        result = result.rotate(1, Rotation.Xleft)
    assert uut == result
    assert uut.getCubelets()[1, 1, 1] == result.getCubelets()[1, 1, 1]
    assert uut.getCubelets() == result.getCubelets()


def test_cube_rotation_z1_leaves_others_alone():
    uut = Cube()
    result = uut.rotate(1, Rotation.Zleft)
    assert uut.getCubeletsInLayer(0, Rotation.Zleft) == \
        result.getCubeletsInLayer(0, Rotation.Zleft)
    assert uut.getCubeletsInLayer(-1, Rotation.Zleft) == \
        result.getCubeletsInLayer(-1, Rotation.Zleft)


def test_cube_rotation_z0_leaves_others_alone():
    uut = Cube()
    result = uut.rotate(0, Rotation.Zleft)
    assert uut.getCubeletsInLayer(1, Rotation.Zleft) == \
        result.getCubeletsInLayer(1, Rotation.Zleft)
    assert uut.getCubeletsInLayer(-1, Rotation.Zleft) == \
        result.getCubeletsInLayer(-1, Rotation.Zleft)


def test_cube_rotation_z_1_leaves_others_alone():
    uut = Cube()
    result = uut.rotate(-1, Rotation.Zleft)
    assert uut.getCubeletsInLayer(1, Rotation.Zleft) == \
        result.getCubeletsInLayer(1, Rotation.Zleft)
    assert uut.getCubeletsInLayer(1, Rotation.Zleft) == \
        result.getCubeletsInLayer(1, Rotation.Zleft)


def test_cube_rotation_z1_left():
    uut = Cube()
    result = uut.rotate(1, Rotation.Zleft)
    result_cubelets = result.getCubelets()
    # Ensure that all nine cubes have been rotated correctly,
    # in both space and orientation
    assert result_cubelets[(1, 1, 1)] == Cubelet(Face(Color.White, zn),
                                                 Face(Color.Green, yn),
                                                 Face(Color.Red, xn))
    assert result_cubelets[(1, 0, 1)] == Cubelet(Face(Color.White, zn),
                                                 Face(Color.Red, xn))
    assert result_cubelets[(1, -1, 1)] == Cubelet(Face(Color.White, zn),
                                                  Face(Color.Yellow, inv(yn)),
                                                  Face(Color.Red, xn))
    assert result_cubelets[(0, 1, 1)] == Cubelet(Face(Color.White, zn),
                                                 Face(Color.Green, yn))
    assert result_cubelets[(0, 0, 1)] == Cubelet(Face(Color.White, zn))
    assert result_cubelets[(0, -1, 1)] == Cubelet(Face(Color.White, zn),
                                                  Face(Color.Yellow, inv(yn)))
    assert result_cubelets[(-1, 1, 1)] == Cubelet(Face(Color.White, zn),
                                                  Face(Color.Green, yn),
                                                  Face(Color.Blue, inv(xn)))
    assert result_cubelets[(-1, 0, 1)] == Cubelet(Face(Color.White, zn),
                                                  Face(Color.Blue, inv(xn)))
    assert result_cubelets[(-1, -1, 1)] == Cubelet(Face(Color.White, zn),
                                                   Face(Color.Yellow, inv(yn)),
                                                   Face(Color.Blue, inv(xn)))


def test_cube_rotation_z0_left():
    uut = Cube()
    result = uut.rotate(0, Rotation.Zleft)
    result_cubelets = result.getCubelets()
    # Ensure that all nine cubes have been rotated correctly,
    # in both space and orientation
    assert result_cubelets[(1, 1, 0)] == Cubelet(Face(Color.Green, yn),
                                                 Face(Color.Red, xn))
    assert result_cubelets[(1, 0, 0)] == Cubelet(Face(Color.Red, xn))
    assert result_cubelets[(1, -1, 0)] == Cubelet(Face(Color.Yellow, inv(yn)),
                                                  Face(Color.Red, xn))
    assert result_cubelets[(0, 1, 0)] == Cubelet(Face(Color.Green, yn))
    assert result_cubelets[(0, -1, 0)] == Cubelet(Face(Color.Yellow, inv(yn)))
    assert result_cubelets[(-1, 1, 0)] == Cubelet(Face(Color.Green, yn),
                                                  Face(Color.Blue, inv(xn)))
    assert result_cubelets[(-1, 0, 0)] == Cubelet(Face(Color.Blue, inv(xn)))
    assert result_cubelets[(-1, -1, 0)] == Cubelet(Face(Color.Yellow, inv(yn)),
                                                   Face(Color.Blue, inv(xn)))


def test_cube_rotation_z_1_left():
    uut = Cube()
    result = uut.rotate(-1, Rotation.Zleft)
    result_cubelets = result.getCubelets()
    # Ensure that all nine cubes have been rotated correctly,
    # in both space and orientation
    assert result_cubelets[(1, 1, -1)] == Cubelet(Face(Color.Purple, inv(zn)),
                                                  Face(Color.Green, yn),
                                                  Face(Color.Red, xn))
    assert result_cubelets[(1, 0, -1)] == Cubelet(Face(Color.Purple, inv(zn)),
                                                  Face(Color.Red, xn))
    assert result_cubelets[(1, -1, -1)] == Cubelet(Face(Color.Purple, inv(zn)),
                                                   Face(Color.Yellow, inv(yn)),
                                                   Face(Color.Red, xn))
    assert result_cubelets[(0, 1, -1)] == Cubelet(Face(Color.Purple, inv(zn)),
                                                  Face(Color.Green, yn))
    assert result_cubelets[(0, 0, -1)] == Cubelet(Face(Color.Purple, inv(zn)))
    assert result_cubelets[(0, -1, -1)] == Cubelet(Face(Color.Purple, inv(zn)),
                                                   Face(Color.Yellow, inv(yn)))
    assert result_cubelets[(-1, 1, -1)] == Cubelet(Face(Color.Purple, inv(zn)),
                                                   Face(Color.Green, yn),
                                                   Face(Color.Blue, inv(xn)))
    assert result_cubelets[(-1, 0, -1)] == Cubelet(Face(Color.Purple, inv(zn)),
                                                   Face(Color.Blue, inv(xn)))
    assert result_cubelets[(-1, -1, -1)] == Cubelet(Face(Color.Purple, inv(zn)),
                                                    Face(
                                                        Color.Yellow, inv(yn)),
                                                    Face(Color.Blue, inv(xn)))


def test_next_neighbours():
    """
    Test the generation of next neighbor information

    In particular:
    - we get 18 neighbors
    - that the layers are in the right range [-1,1]
    - that the structure of the returned list is correct ((layer, Rotation), Cube)
    - that all six rotations are considered
    - that all the returned cubes are distinct from one another, and from the original cube
    """
    uut = Cube()
    n = uut.getNeighbors()
    assert len(n) == 18
    ((l, r), c) = n[0]
    assert -1 <= l and l <= 1
    assert isinstance(r, Rotation)
    assert isinstance(c, Cube)
    assert len(set([r for ((l, r), c) in n])) == 6
    nc = set([c for (_, c) in n])
    nc.add(uut)
    assert len(nc) == 19
