from enum import Enum
import numpy as np


class Color(Enum):
    """
    Represent colors Blue, Red, Green, Yellow, White, Purple
    """
    Blue = 'b'
    Red = 'r'
    Green = 'g'
    Yellow = 'y'
    White = 'w'
    Purple = 'p'


class Cube:
    __slots__ = ['_cubelets', '_hashval']

    def __init__(self, cubelets=None):
        """
        Construct a cube, either from the collection of cubelets or as a new
        Cube.

        If a new Cube is contructed then faces orthogonal to X will be
        Green and Yellow; orthogonal to Y will be Blue and Red; orthogonal to
        Z will be White and Purple, in the positive and negative direction
        respectively.
        """

        # There are 3 planes of 9 cubelets, making 27, of which
        # 1 is completely hidden so can be ignored.
        if cubelets is None:
            self._cubelets = {}
            # Along each axis there are 3 planes of cubelets
            # lying at the -1, 0, 1 locations.
            # For any given axis the faces perpendicular to that
            # axis are given as below. If the location on the axis is at the
            # origin then there is no face with that orientation.
            xf = {1: Face(Color.Green, [1, 0, 0]),
                  0: None, -1: Face(Color.Yellow, [-1, 0, 0])}
            yf = {1: Face(Color.Blue, [0, 1, 0]),
                  0: None,
                  -1: Face(Color.Red, [0, -1, 0])}
            zf = {1: Face(Color.White, [0, 0, 1]),
                  0: None, -1: Face(Color.Purple, [0, 0, -1])}

            for x in (-1, 0, 1):
                for y in (-1, 0, 1):
                    for z in (-1, 0, 1):
                        if (x, y, z) != (0, 0, 0):
                            self._cubelets[(x, y, z)] = Cubelet(
                                xf[x], yf[y], zf[z])
        else:
            self._cubelets = cubelets

        assert len(self._cubelets) == 26

        self._hashval = hash(frozenset(self._cubelets.items()))

    def __hash__(self):
        return self._hashval

    def __eq__(self, other):
        return isinstance(other, self.__class__) and \
            hash(self) == hash(other)

    def getCubelets(self):
        """
        Return the cubelets composing the receiver
        """
        return self._cubelets

    def getCubeletsInLayer(self, layer, rotation):
        return {k: v for k, v in self._cubelets.items()
                if k in self.getCubeletLocationsInLayer(layer, rotation)}

    def getCubeletLocationsInLayer(self, layer, rotation):
        """
        Return the keys (i.e. the 3d coordinates) of the cubelets
        in the layer specified by the layer number and the rotation
        """
        if rotation in [Rotation.Xleft, Rotation.Xright]:
            return [(x, y, z) for (x, y, z) in self._cubelets.keys() if x == layer]
        if rotation in [Rotation.Yleft, Rotation.Yright]:
            return [(x, y, z) for (x, y, z) in self._cubelets.keys() if y == layer]
        if rotation in [Rotation.Zleft, Rotation.Zright]:
            return [(x, y, z) for (x, y, z) in self._cubelets.keys() if z == layer]

    def rotate(self, layer, rotation):
        """
        Return a cube that is like the receiver except the given layer is
        rotated according to the given rotation

        The layer is specified as a layer number, in the range [-1,1],
        perpendicular to the axis of rotation given
        """

        new_cubelets = self._cubelets.copy()
        for k in self.getCubeletLocationsInLayer(layer, rotation):
            # For each of the cubes in the layer its location in
            # 3d space must be rotated, and it too must be rotated
            new_cubelets[tuple(np.dot(k, rotation.value))] = \
                self._cubelets[k].rotate(rotation)
        return Cube(new_cubelets)

    def getNeighbors(self):
        """
        Return a list of pairs, the first of which is a rotation and the second the
        cube to which the given move would rotate the receiver
        """
        return [((l, r), self.rotate(l, r)) for l in [1, 0, -1] for r in Rotation]


class CubeRotation:
    def __init__(self, layer, rotation):
        self.layer = layer
        self.rotation = rotation

    def __str__(self): return "l{} {}".format(self.layer, self.rotation)


class Cubelet:
    __slots__ = ['faces', '_hashval']
    """
    A cubelet is the smallest cube that a Rubik's Cube is comprised of.

    It is characterized by having between 1 and 3 Faces, orthogonal to one
    another. Their relative position is fixed, and this is the invariant
    that a Cubelet maintains.

    Cubelets are immutable.
    """

    def __init__(self, face1, face2=None, face3=None):
        """
        Construct a cube from between 1 and three Faces, each unique in color,
        orientation and axis
        """
        self.faces = [f for f in filter(None.__ne__, [face1, face2, face3])]
        # If the array of faces forms a set with less members then some
        # of the members aren't unique in color and orientation. They're the
        # same face.
        if len(self.faces) != len(set(self.faces)):
            raise SameFacesError

        colors = [f.color for f in self.faces]
        # If we look at just the colors they should be unique
        if len(colors) != len(set(colors)):
            raise SameColorDifferentOrientationError

        # Check that for any pair of faces they're not opposite one another
        for this in self.faces:
            for that in [t for t in self.faces if this != t]:
                if this.isOpposite(that):
                    raise OppositeFacesError

        # Check that no two faces have the same orientation
        if len(self.faces) != len(set([f.orientation for f in self.faces])):
            raise SameOrientationError

        self.faces = frozenset(self.faces)
        self._hashval = hash(self.faces)

    def __eq__(self, other):
        return isinstance(other, self.__class__) and \
            hash(self) == hash(other)

    def __hash__(self):
        return self._hashval

    def __repr__(self): return ' '.join(map(repr, self.faces))

    def rotate(self, m):
        return Cubelet(*[f.rotate(m) for f in self.faces])

    def getFaceWithColor(self, color):
        for f in self.faces:
            if f.color == color:
                return f
        return None


class Face:
    __slots__ = ['color', 'orientation']
    """
    A Face represents the face of a cubelet. It has a color and an orientation,
    expressed as the normal of the plane of the face.

    Faces are immutable
    """

    def __init__(self, color, orientation):
        self.color = color
        self.orientation = tuple(orientation)

    def isOpposite(self, other=None):
        return isinstance(other, self.__class__) and \
            np.dot(self.orientation, other.orientation) == -1

    def rotate(self, rotation):
        return Face(self.color, tuple(np.dot(self.orientation, rotation.value)))

    def __hash__(self): return hash(self.color) * hash(self.orientation)

    def __eq__(self, other):
        return isinstance(other, self.__class__) and \
            self.color == other.color and self.orientation == other.orientation

    def __str__(self): return '{} {}'.format(self.color, self.orientation)

    def __repr__(self): return 'Face({}, {})'.format(
        repr(self.color), repr(self.orientation))


class OppositeFacesError(Exception):
    pass

# Here we capture the Right or Left hand rotation rule


class Rotation(Enum):
    """
    The rotation matrix for a right handed coordinate system around each of the
    major axes
    """
    Xleft = ((1, 0, 0), (0, 0, 1), (0, -1, 0))
    Xright = ((1, 0, 0), (0, 0, -1), (0, 1, 0))
    Yleft = ((0, 0, -1), (0, 1, 0), (1, 0, 0))
    Yright = ((0, 0, 1), (0, 1, 0), (-1, 0, 0))
    Zleft = ((0, 1, 0), (-1, 0, 0), (0, 0, 1))
    Zright = ((0, -1, 0), (1, 0, 0), (0, 0, 1))


class SameColorDifferentOrientationError(Exception):
    pass


class SameFacesError(Exception):
    pass


class SameOrientationError(Exception):
    pass


def runit():
    seen = set()

    cubes = [Cube()]
    seen.add(cubes[0])

    nc = 0
    for cube in cubes:
        # print("len(cubes): {}; len(seen): {}".format(len(cubes), len(seen)))

        nc += 1
        # print(nc)
        if nc == 1000:
            nc = 0
            print("len(cubes): {}; len(seen): {}".format(len(cubes), len(seen)))

        new_cubes = [c for (_, c) in cube.getNeighbors()]
        unseen = [c for c in new_cubes if c not in seen]
        # print("Adding {} cubes".format(len(unseen)))
        cubes.extend(unseen)
        seen.update(unseen)


# runit()
def runForN(num=100, pi=1000):
    seen = set()

    cubes = [Cube()]
    seen.add(cubes[0])

    nc = 0
    for cube in cubes:
        # print("len(cubes): {}; len(seen): {}".format(len(cubes), len(seen)))

        nc += 1
        # print(nc)
        if nc % pi == 0:
            print("len(cubes): {}; len(seen): {}".format(len(cubes), len(seen)))
        if nc >= num:
            print("len(cubes): {}; len(seen): {}".format(len(cubes), len(seen)))
            return

        new_cubes = [c for (_, c) in cube.getNeighbors()]
        unseen = [c for c in new_cubes if c not in seen]
        # print("Adding {} cubes".format(len(unseen)))
        cubes.extend(unseen)
        seen.update(unseen)


runForN()
