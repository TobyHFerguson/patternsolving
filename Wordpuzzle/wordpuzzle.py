from enum import Enum
import copy
import sys
import itertools


class Board:
    top = 0
    left = 0

    def __init__(self, content):
        """content is a square array and will be used to initialize the board.

        The number 0 represents the hole in the board
        """
        self.content = content
        self.bottom = len(content) - 1
        self.right = len(content[0]) - 1
        num_holes = len(self.get_holes())
        if (num_holes == 0):
            raise NoHoleError
        elif (num_holes > 1):
            raise MultipleHolesError

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __repr__(self):
        return "Board({})".format(self.content)

    def __str__(self):
        result = ""
        for row in self.content:
            for cell in row:
                result += str(cell)
        return result

    def __hash__(self):
        return hash(self.__repr__())

    def get_holes(self):
        return [(i, j) for i in range(len(self.content))
                for j in range(len(self.content[i]))
                if self.content[i][j] == 0]

    # def get_moves(self):
    #     result = set()
    #     hole = self.get_holes()[0]
    #     if hole[0] > self.top:
    #         result.add(Move.Up)
    #     if hole[0] < self.bottom:
    #         result.add(Move.Down)
    #     if hole[1] > self.left:
    #         result.add(Move.Left)
    #     if hole[1] < self.right:
    #         result.add(Move.Right)
    #     return result
    def get_moves(self):
        result = []
        hole = self.get_holes()[0]
        if hole[0] > self.top:
            result.append(Move.Up)
        if hole[0] < self.bottom:
            result.append(Move.Down)
        if hole[1] > self.left:
            result.append(Move.Left)
        if hole[1] < self.right:
            result.append(Move.Right)
        return result

    def get_neighbors(self):
        """Return a dictionary linking each move to be made from this block to the resulting neighbor"""
        moves = self.get_moves()
        result = {}
        for move in moves:
            result[move] = self.move(move)
        return result

    def get_content(self):
        return self.content

    def move(self, direction):
        c = copy.deepcopy(self)
        mover = {
            Move.Up: c._up,
            Move.Down: c._down,
            Move.Left: c._left,
            Move.Right: c._right
        }
        mover.get(direction)()
        return c

    def _up(self):
        hole = self.get_holes()[0]
        if hole[0] == 0:
            raise InvalidMoveError(Move.Up)
        else:
            self._swap(hole, (hole[0] - 1, hole[1]))

    def _down(self):
        hole = self.get_holes()[0]
        if hole[0] == len(self.content) - 1:
            raise InvalidMoveError(Move.Down)
        else:
            self._swap(hole, (hole[0] + 1, hole[1]))

    def _left(self):
        hole = self.get_holes()[0]
        if hole[1] == 0:
            raise InvalidMoveError(Move.Left)
        else:
            self._swap(hole, (hole[0], hole[1] - 1))

    def _right(self):
        hole = self.get_holes()[0]
        if hole[1] == len(self.content[0]) - 1:
            raise InvalidMoveError(Move.Right)
        else:
            self._swap(hole, (hole[0], hole[1] + 1))

    def _swap(self, this, other):
        x = self.content[this[0]][this[1]]
        self.content[this[0]][this[1]] = self.content[other[0]][other[1]]
        self.content[other[0]][other[1]] = x


class Move(Enum):
    Left = 'l'
    Right = 'r'
    Up = 'u'
    Down = 'd'

    def __str__(self): return '{}'.format(self.value)


class InvalidMoveError(Exception):
    def __init__(self, move): self.move = move


class NoHoleError(Exception):
    pass


class MultipleHolesError(Exception):
    pass


def neighbors_with_history(board, history):
    """
    Return a generator containing tuples, each containing a neighbor of
    the given board and the list of moves required to get to that board

    board - an instance of Board

    history - a list of Move, with the latest move at the end of the list.
    """
    assert history is not None
    assert type(history) == tuple
    return ((b, history + (m,))
            for (m, b) in board.get_neighbors().items())


def new_neighbors_only(board_history_generator, explored):
    """Return a board history generator where none of the boards returned
    is in the set of explored boards"""
    return ((b, h) for (b, h) in board_history_generator
            if b not in explored)


class MyStream:
    def __init__(self, board_history_stream, explored):
        self.stream = board_history_stream
        self.explored = explored
        self.children = None

    def __iter__(self): return self

    def __next__(self):
        (board, history) = self.myNext()
        neighbors = neighbors_with_history(board, history)
        nno = new_neighbors_only(neighbors, self.explored)
        self.explored.add(board)
        if self.children is None:
            self.children = nno
        else:
            self.children = itertools.chain(self.children, nno)
        return (board, history)

    def myNext(self):
        try:
            return next(self.stream)
        except StopIteration:
            if self.children is not None:
                self.stream = self.children
                return next(self.stream)
            else:
                raise StopIteration


def main():
    board0 = Board([[1, 2, 3], [4, 0, 6], [7, 8, 9]])
    board = Board([[0, 1], [1, 1]])
    board2 = Board([[0, 1], [2, 3]])
    board = Board([[0, 2, 3], [4, 5, 6], [7, 8, 9]])
    board = Board([[0, 1, 1], [1, 1, 1], [1, 1, 1]])
    bhg = ((b, h) for (b, h) in [(board, ())])
    i = 0
    for (b, m) in MyStream(bhg, set()):
        print("({}, {})".format(b, [str(i) for i in m]))
        i += 1
    print(i)


if __name__ == "__main__":
    sys.exit(main())
